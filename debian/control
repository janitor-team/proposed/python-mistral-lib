Source: python-mistral-lib
Section: python
Priority: optional
Maintainer: Debian OpenStack <team+openstack@tracker.debian.org>
Uploaders:
 Thomas Goirand <zigo@debian.org>,
Build-Depends:
 debhelper-compat (= 11),
 dh-python,
 openstack-pkg-tools,
 python3-all,
 python3-pbr,
 python3-setuptools,
 python3-sphinx,
Build-Depends-Indep:
 python3-eventlet,
 python3-openstackdocstheme,
 python3-oslo.log,
 python3-oslo.serialization,
 python3-oslotest,
 python3-stestr,
 python3-testtools,
 python3-pygments,
 python3-yaql,
 subunit,
Standards-Version: 4.5.1
Vcs-Browser: https://salsa.debian.org/openstack-team/libs/python-mistral-lib
Vcs-Git: https://salsa.debian.org/openstack-team/libs/python-mistral-lib.git
Homepage: http://docs.openstack.org/developer/mistral/

Package: python-mistral-lib-doc
Section: doc
Architecture: all
Depends:
 ${misc:Depends},
 ${sphinxdoc:Depends},
Description: Mistral shared routings and utilities - doc
 This library contains data types, exceptions, functions and utilities common
 to Mistral, python-mistralclient and mistral-extra repositories.  This library
 also contains the public interfaces for 3rd party integration (e.g. Actions
 API, YAQL functions API, etc.)
 .
 If you want to use OpenStack in your custom actions or functions, you will
 also need to use http://git.openstack.org/cgit/openstack/mistral-extra.
 .
 This package contains the documentation.

Package: python3-mistral-lib
Architecture: all
Depends:
 python3-eventlet,
 python3-oslo.log,
 python3-oslo.serialization,
 python3-pbr,
 python3-yaql,
 ${misc:Depends},
 ${python3:Depends},
Suggests:
 python-mistral-lib-doc,
Description: Mistral shared routings and utilities - Python 3.x
 This library contains data types, exceptions, functions and utilities common
 to Mistral, python-mistralclient and mistral-extra repositories.  This library
 also contains the public interfaces for 3rd party integration (e.g. Actions
 API, YAQL functions API, etc.)
 .
 If you want to use OpenStack in your custom actions or functions, you will
 also need to use http://git.openstack.org/cgit/openstack/mistral-extra.
 .
 This package contains the Python 3.x module.
